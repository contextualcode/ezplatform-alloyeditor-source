<?php

namespace ContextualCode\EzPlatformAlloyEditorSource\Configuration\Provider;

use eZ\Publish\Core\Base\Exceptions\InvalidArgumentValue;
use eZ\Publish\Core\MVC\Symfony\Security\Authorization\Attribute;
use EzSystems\EzPlatformRichText\Configuration\Provider\AlloyEditor;
use EzSystems\EzPlatformRichText\SPI\Configuration\Provider;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;

final class ExtendedAlloyEditor implements Provider
{
    /** @var AlloyEditor */
    private $alloyEditor;

    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    public function __construct(
        AlloyEditor $alloyEditor,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->alloyEditor = $alloyEditor;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function getName(): string
    {
        return $this->alloyEditor->getName();
    }

    public function getConfiguration(): array
    {
        $configuration = $this->alloyEditor->getConfiguration();
        $configuration['buttonPermissions'] = $this->getButtonPermissions($configuration);

        return $configuration;
    }

    private function getButtonPermissions(array $configuration): array
    {
        $permissions = [];

        foreach ($configuration['toolbars'] as $toolbar => $definition) {
            foreach ($definition['buttons'] as $button) {
                if (isset($permissions[$button])) {
                    continue;
                }

                $permissions[$button] = $this->isButtonEnabled($button);
            }
        }

        return $permissions;
    }

    private function isButtonEnabled(string $button): bool
    {
        $attributes = new Attribute('content', 'onlineeditor.' . $button);
        try {
            $isEnabled = $this->authorizationChecker->isGranted($attributes);
        } catch (InvalidArgumentValue | AuthenticationCredentialsNotFoundException $e) {
            $isEnabled = true;
        }

        return $isEnabled;
    }
}
