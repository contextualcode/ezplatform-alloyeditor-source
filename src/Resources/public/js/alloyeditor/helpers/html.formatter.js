export default class HtmlFormatter {
    constructor() {
    }

    format(str) {
        const div = document.createElement('div');
        div.innerHTML = str.trim();

        return this.formatElement(div, 0).innerHTML.trim();
    };

    formatElement(node, level) {
        let indentBefore = new Array(level++ + 1).join('  '),
            indentAfter  = new Array(level - 1).join('  '),
            textNode;

        for (let i = 0; i < node.children.length; i++) {
            textNode = document.createTextNode('\n' + indentBefore);
            node.insertBefore(textNode, node.children[i]);

            this.formatElement(node.children[i], level);

            if (node.lastElementChild == node.children[i]) {
                textNode = document.createTextNode('\n' + indentAfter);
                node.appendChild(textNode);
            }
        }

        return node;
    };
}