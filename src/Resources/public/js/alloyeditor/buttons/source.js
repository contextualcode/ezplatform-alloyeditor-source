import React from 'react';
import ReactDOM from 'react-dom';
import AlloyEditor from 'alloyeditor';
import EzButton from './../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/base/ez-button';
import SourceModal from '../modals/source';

class BtnSource extends EzButton {
    static get key() {
        return 'source';
    }

    constructor(props) {
        super(props);

        this.container = document.querySelector('.ez-modal-wrapper');
    }

    updateSourceCode(html) {
        const editor = this.props.editor.get('nativeEditor');
        // [IE8] Focus editor before setting selection to avoid setting data on
        // locked selection, because in case of inline editor, it won't be
        // unlocked before editable's HTML is altered. (https://dev.ckeditor.com/ticket/11585)
        editor.focus();
        editor.setData(html, () => {
            // Ensure correct selection.
            const range = editor.createRange();
            range.moveToElementEditStart(editor.editable());
            range.select();
        } );
    }

    editSource() {
        const editor = this.props.editor.get('nativeEditor');
        const config = {
            html: editor.getData(),
            onConfirm: this.updateSourceCode.bind(this),
            onClose: () => {},
        };
        ReactDOM.render(React.createElement(SourceModal, config), this.container);
    }

    /**
     * Lifecycle. Renders the UI of the button.
     *
     * @method render
     * @return {Object} The content which should be rendered.
     */
    render() {
        return (
            <button
                disabled={!window.eZ.richText.alloyEditor.buttonPermissions.source}
                className="ae-button ez-btn-ae ez-btn-ae--source"
                onClick={this.editSource.bind(this)}
                tabIndex={this.props.tabIndex}
                title={Translator.trans('button.title', {}, 'alloy_editor_source')}
            >
                <svg className="ez-icon ez-btn-ae__icon">
                    <use xlinkHref="/bundles/ezplatformalloyeditorsource/img/icons.svg#edit-source" />
                </svg>
            </button>
        );
    }
}

AlloyEditor.Buttons[BtnSource.key] = AlloyEditor.BtnSource = BtnSource;